import React, {Component, useState} from 'react';
import { StyleSheet,TextInput, TouchableOpacity, Text, View, Image } from 
'react-native';
import MyList from './app/components/mylist/MyList'
import AgeValidator from './app/components/agevalidator/AgeValidator'

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = { 
        textValue: '',
      count: 0,
    };
  }

  changeTextInput = text => {
    let newText = '';
    let numbers = '0123456789';

    for (var i=0; i < text.length; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
            newText = newText + text[i];
        }
        else {
            // your call back function
            alert("Porfavor ingrese solo valores numericos");
        }
    }
    this.setState({ myNumber: newText });
  };

  evaluar = newText =>{
    parseInt(newText) >= 18? (
      alert("Eres mayor de edad.")
    ):(
      alert("Eres menor de edad.")
    )
  }
  
  render(){
    return(
      <View style={styles.container}>
        <View>
          <Text>Componente AgeValidator.js</Text>
        </View>

        <View style={styles.text}>
          <Text>Ingrese su Edad</Text>
        </View>
        
        <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        keyboardType='numeric'
        onChangeText={text => this.changeTextInput(text)}
        value={this.state.myNumber}
        maxLength= {10}
        />

        <AgeValidator text={'Texto en Body'} onBodyPress={this.evaluar}
        />

        <View>
          <Text>Componente MyList.js</Text>
        </View>

        <MyList />

      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    backgroundColor: '#99BCB5',
  },
  text: {
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button:{
    top:10,
    alignItems:'center',
    backgroundColor:'#DAEAE7',
    padding:10,
    marginBottom:100,
  },
  countContainer: {
    alignItems:'center',
    padding: 10,
  },

  image:{
    marginLeft: 35,
  }
});


