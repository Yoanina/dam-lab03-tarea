import React from "react";
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar,Image } 
from 'react-native';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Primer Elemento',
    Image: './app/img/logo.png',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Segundo Elemento',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Tercer Elemento',
    
  },
  {
    id: '58694a0f-3da1-471f-bd96-37733390i130',
    title: 'Cuarto Elemento',
  },
  {
    id: '58694a0f-3da1-471f-bd96-182873878288',
    title: 'Quinto Elemento',
  },
];

const Item = ({ title }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
  </View>
);

const MyList = () => {
  const renderItem = ({ item }) => (
    <View>
    <Item title={item.title} />
    <Image
          style={{ width: 90, height: 67, marginBottom: 10, marginTop:-75 }}
          source={require("./img/img1.png")}/> 
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>  
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',padding: 20,
    width:300,marginLeft:100,
    marginVertical: 8,marginHorizontal: 16,
  },
  title: {
    fontSize: 20,
  },
});

export default MyList;