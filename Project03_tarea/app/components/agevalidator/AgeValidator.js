import React from "react";

import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const AgeValidator = props => (
    <View>
        <Text>{props.textBody}</Text>
        <TouchableOpacity style={styles.button} onPress={props.onBodyPress} >
            <Text>Evaluar Edad</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    button: {
        top:10,
        alignItems: 'center',
        backgroundColor: '#DDDDDD',
        padding: 10,
        marginBottom:60
    },
});

export default AgeValidator;


